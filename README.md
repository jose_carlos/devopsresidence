# DjangoOnDocker Project

A basic project to install and view Django web app in simple way.
## Requirements
- Install [Docker](https://docs.docker.com/engine/install/)
- (Optional) ✨ [VSCode](https://code.visualstudio.com/) ✨

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Starting a New Project

Django is very easy to install and deploy in a Docker container.

the Docker will expose port 8787, so change this within the
Docker-compose file if necessary. When ready, simply use the Docker-compose file to
build the image.

#### Windows
```sh
docker-compose run web django-admin startproject DjangoOnDocker .
```
#### Linux

```sh
sudo docker-compose run web django-admin startproject DjangoOnDocker .
```
## Acessing Django
```sh
docker-compose up
```


And that's it!

## Running MyWebSite Project

Install the dependencies and start the server.

```sh
http://localhost:8787/hello
```

## Creating a new Django App

Install the dependencies and start the server.
```sh
docker exec devopsresidence_web_1 python manage.py startapp {NAME APP}
```