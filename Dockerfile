FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /DjangoApp
WORKDIR /DjangoApp
COPY requirements.txt /DjangoApp/
RUN pip install -r requirements.txt
COPY . /DjangoApp/

